 
class TokenPacket extends TransactionBase;
  rand bit [4:0] Crc5;
  rand bit [3:0] Endp;
  rand bit [6:0] Addr;
  rand Pid_t Pid;

  `ovm_object_utils_begin( TokenPacket)
    `ovm_field_enum(Pid_t,Pid,OVM_ALL_ON)
    `ovm_field_int(Endp,OVM_ALL_ON)
    `ovm_field_int(Addr,OVM_ALL_ON)
    `ovm_field_int(Crc5,OVM_ALL_ON)
  `ovm_object_utils_end
  
  extern function new( string name = "" );
  extern function void CalCrc5();
  constraint Token_Pid;

endclass : TokenPacket

function TokenPacket::new( string name = "");
  super.new(name);
endfunction : new

function void TokenPacket::CalCrc5();
  bit [10:0] d = {Addr[0],Addr[1],Addr[2],Addr[3],Addr[4],Addr[5],Addr[6],Endp[0],Endp[1],Endp[2],Endp[3]};
  bit [4:0] c = 5'b11111;
  bit [4:0] b = 5'b00101;
  //$display("%b",d);
  for(int i=10;i>=0;i--) begin
    if(c[4]==d[i]) begin
      c = {c[3:0],1'b0};
    end else begin
      c = {c[3:0],1'b0};
      c = c^b;
    end
  end
  Crc5 = ~c;
  //$display("%b",Crc5);
endfunction : CalCrc5

constraint TokenPacket::Token_Pid{ Pid inside {OUT,IN,SOF,SETUP};}



 
class Status_Transaction extends ovm_sequence #( TransactionBase );
  `ovm_object_utils_begin( Status_Transaction )
  `ovm_object_utils_end

  extern function new( string name = "" );
  extern task body();
  
  
endclass : Status_Transaction

function Status_Transaction::new( string name = "" );
  super.new( name );
endfunction : new

task Status_Transaction::body();
  TokenPacket Packet1;
  bit [0:63] add;
  DataPacket Packet2;
  HandShakePacket Packet3;
  TransactionBase Packet;
  Packet2 = DataPacket::type_id::create( "Data" );
  Packet1 = TokenPacket::type_id::create( "Token" );

  //out phase for the set address command
  wait_for_grant();
  void'( Packet1.randomize() with { Packet1.Pid == IN;
                                   Packet1.Addr == 7'b0000000;
                                   Packet1.Endp == 4'b0000;    });
  Packet1.CalCrc5();
  send_request( Packet1 );
  wait_for_item_done();
  
  get_response( Packet2 );
   if (Packet2.Pid == DATA1)
  ovm_report_info(get_type_name,"Received zero length packet from the device...",OVM_MEDIUM);
  else 
  ovm_report_info(get_type_name,"No data or device is busy ...",OVM_MEDIUM);
  
  
  ovm_report_info(get_type_name,"Sending ack from host...",OVM_MEDIUM);
  wait_for_item_done();
  
  ovm_report_info(get_type_name,"Received acknowledgement from the device...",OVM_MEDIUM);

  // ACK phase 
  wait_for_grant();
  Packet3 = HandShakePacket::type_id::create("HandShake");
  void'(Packet3.randomize() with { Packet3.Pid == ACK;});
  send_request( Packet3 );
  wait_for_item_done();


 endtask : body


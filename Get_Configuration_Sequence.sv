   
class Get_Configuration_Sequence extends ovm_sequence#( TransactionBase );
  `ovm_object_utils_begin( Get_Configuration_Sequence )
  `ovm_object_utils_end

  extern function new( string name = "" );
  extern task body();
  
endclass : Get_Configuration_Sequence

function Get_Configuration_Sequence::new( string name = "" );
  super.new( name );
endfunction : new

task Get_Configuration_Sequence::body();
  TokenPacket Packet1;                                                                                       
  bit [0:63] con;
  DataPacket Packet2;
  HandShakePacket Packet3;
  TransactionBase Packet;
  Packet2 = DataPacket ::type_id::create( "Data"  );
  Packet1 = TokenPacket::type_id::create( "Token" );
  
  //setup phase for the Set Configuration commands
  wait_for_grant();
  void'( Packet1.randomize() with { Packet1.Pid == SETUP;
                                    Packet1.Addr == 7'b0000001;
                                    Packet1.Endp == 4'b0000;    });
  Packet1.CalCrc5();
  send_request( Packet1 );
  wait_for_item_done();
  wait_for_grant();
  void'( Packet2.randomize() with { Packet2.Pid == DATA0;});
  con = 64'h80_08_00_00_00_00_01_00;
  Packet2.Data = new[64];
  foreach( con[i] ) begin
    Packet2.Data[i] = con[i];
  end
  Packet2.CalCrc16();
  send_request( Packet2 );
  ovm_report_info(get_type_name,"Sending Command Get Configuration...",OVM_MEDIUM);
  wait_for_item_done();
  ovm_report_info(get_type_name,"Received acknowledgement from the device...",OVM_MEDIUM);
  
  //Data Phase for the get configuration commands
 
  wait_for_grant();
  get_response(Packet);
  Packet2 = DataPacket ::type_id::create( "Data"  );
  Packet1 = TokenPacket::type_id::create( "Token" );
  
  void'( Packet1.randomize() with { Packet1.Pid == IN;
                                    Packet1.Addr == 7'b0000001;
                                    Packet1.Endp == 4'b0000;    });
  Packet1.CalCrc5();
  send_request( Packet1 );
  wait_for_item_done();
  wait_for_grant();
  get_response( Packet );
  Packet3 = HandShakePacket::type_id::create( "HandShake" );
  void'( Packet3.randomize() with { Packet3.Pid == ACK; });
  send_request( Packet3 );
  wait_for_item_done();                                                                                    
  
  //status phase for the get configuration command
  Packet2 = DataPacket ::type_id::create( "Data"  );
  Packet1 = TokenPacket::type_id::create( "Token" );
  wait_for_grant();
  void'(Packet1.randomize() with { Packet1.Pid == OUT;
                                   Packet1.Addr == 7'b0000001;
                                   Packet1.Endp == 4'b0000;});                                               
  Packet1.CalCrc5();
  send_request( Packet1 );
  wait_for_item_done();
  ovm_report_info(get_type_name,"Sent Data to device ...",OVM_MEDIUM);
  wait_for_grant();
  //Packet3 = HandShakePacket::type_id::create( "HandShake" );
  void'( Packet2.randomize() with { Packet2.Pid == DATA1; });
  Packet2.Data = new[0];
  Packet2.CalCrc16();
  send_request( Packet2 );
  wait_for_item_done();                                                                                    
  get_response(Packet); 
endtask : body


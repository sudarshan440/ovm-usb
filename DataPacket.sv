
class DataPacket extends TransactionBase;

  bit [15:0] Crc16;
  rand bit Data[];
  rand Pid_t Pid;

  `ovm_object_utils_begin( DataPacket )
    `ovm_field_enum( Pid_t, Pid, OVM_ALL_ON )
    `ovm_field_array_int( Data, OVM_ALL_ON )
    `ovm_field_int( Crc16, OVM_ALL_ON )
  `ovm_object_utils_end
  
  extern function new( string name = "");
  extern function void CalCrc16();
  constraint Data_Pid;
  
endclass : DataPacket

function DataPacket::new( string name = "");
  super.new(name);
endfunction : new

function void DataPacket::CalCrc16();
  bit [15:0] c = 16'hffff;
  //bit da[] = Data;
  bit [15:0] d = 16'b1000000000000101;
  bit temp[0:7];
  for(int i = 0;i<Data.size/8;i++) begin
    temp = Data[i*8+:8];
    temp.reverse;
    foreach(temp[i]) begin
     if(c[15]==temp[i]) begin
       c = {c[14:0],1'b0};
     end else begin
       c = {c[14:0],1'b0};
       c = c^d;
     end
    end
  end
  Crc16 = ~c;
  //$display("");
  //$display("%b",Crc16);
endfunction : CalCrc16

constraint DataPacket::Data_Pid{ Pid inside {DATA0,DATA1,DATA2,MDATA};}


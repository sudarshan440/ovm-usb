class usb_agent extends ovm_agent;
usb_monitor  monwrite; 
usb_monitor  monread; 
usb_driver driver;
usb_sequencer sequencer;
ovm_analysis_port #(TransactionBase) agentwrite ;
ovm_analysis_port #(TransactionBase) agentread ;
 

//registering
`ovm_component_utils(usb_agent)

//constructor
function new (string name = " ", ovm_component parent = null);
super.new(name,parent);

endfunction : new

function void build();
super.build();
agentwrite  = new("agentwrite" ,this);
agentread  = new("agentread" ,this);
 
sequencer = usb_sequencer::type_id::create("sequencer",this);
driver = usb_driver::type_id::create("driver",this);
monwrite = usb_monitor::type_id::create("monwrite",this);
monread = usb_monitor::type_id::create("monread",this);
endfunction : build

function void connect();
driver.seq_item_port.connect(sequencer.seq_item_export);
monwrite.Put1.connect(agentwrite);
monread.Put2.connect(agentread);
 
endfunction : connect


endclass : usb_agent
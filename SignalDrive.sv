class SignalDrive extends ovm_component;
  ovm_blocking_get_port#( DArray ) Get;
  virtual usb_intf intf;
Wrapper wrapper;
ovm_object dummy;

  `ovm_component_utils_begin(SignalDrive) 
   `ovm_component_utils_end 
  extern function new ( string name = "", ovm_component parent = null );
  extern function void build();
  extern task run();
  
endclass  :SignalDrive

function SignalDrive::new( string name = "", ovm_component parent = null );
  super.new(name , parent);
endfunction : new

 

 function void SignalDrive::build();
//casting
if(!get_config_object("configuration",dummy,0))
	ovm_report_info(get_type_name(),"dummy is not assigned",OVM_LOG);
else
	ovm_report_info(get_type_name(),"dummy is assigned",OVM_LOG); 


if($cast(wrapper,dummy))
  begin
	ovm_report_info(get_type_name(),"object configured properly",OVM_LOG);
	intf = wrapper.intf;
  end
else
	ovm_report_info(get_type_name(),"dummy is really dummy");

   Get = new("Get",this);
endfunction : build



task SignalDrive::run();
  DArray tmp;
  intf.usb_p_in      = 1'b1;
  intf.usb_n_in      = 1'b1;
   
  forever begin
    Get.get( tmp );
    //$display("%d",tmp.BitArray.size);
    for(int i = 0; i<tmp.BitArray.size;i++) begin
      @(posedge intf.clk)
      intf.usb_p_in     = tmp.BitArray[i];
      intf.usb_n_in     = ~tmp.BitArray[i];
      //usbInterface.rxd_in_i = 1'b1;
      //$write("%b",tmp.BitArray[i]);
    end
    //$display("");
     @(posedge intf.clk)
    intf.usb_p_in      = 1'b0;
    intf.usb_n_in      = 1'b0;
     
    @(posedge intf.clk)
    intf.usb_p_in      = 1'b0;
    intf.usb_n_in     = 1'b0;
     
    @(posedge intf.clk)
    intf.usb_p_in     = 1'b1;
    intf.usb_n_in     = 1'b0;
     
    ovm_report_info(get_type_name,$psprintf("Stream Injection through interface complete"),OVM_HIGH);
    @(posedge intf.clk);
    @(posedge intf.clk);
    @(posedge intf.clk);
    @(posedge intf.clk);
    @(posedge intf.clk);
    
  end
endtask : run



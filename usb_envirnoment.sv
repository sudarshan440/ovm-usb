class usb_environment extends ovm_env;
usb_agent agent;
scoreboard sb;
//registering
`ovm_component_utils(usb_environment)

//constructor
function new(string name = " ", ovm_component parent = null);
super.new(name,parent);
endfunction: new

function void build();
super.build();
agent = usb_agent::type_id::create("agent",this);
sb = scoreboard::type_id::create("sb",this);
endfunction : build

function void connect();
agent.agentwrite.connect(sb.writesb);
agent.agentread.connect(sb.readsb);
endfunction : connect 

endclass : usb_environment
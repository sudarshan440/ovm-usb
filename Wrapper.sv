class Wrapper extends ovm_object;
virtual usb_intf intf;

`ovm_object_utils(Wrapper)

function new(string name = " " );
super.new(name);
endfunction : new

function setVintf(virtual usb_intf intf);
 this.intf=intf;
endfunction:setVintf 

endclass : Wrapper 
 
class Token extends ovm_sequence #( TransactionBase );
  `ovm_object_utils_begin( Token )
  `ovm_object_utils_end

  extern function new( string name = "" );
  extern task body();
  
  
endclass : Token

function Token::new( string name = "" );
  super.new( name );
endfunction : new

task Token::body();
  TokenPacket Packet1;
  bit [0:63] add;
  DataPacket Packet2;
  HandShakePacket Packet3;
  TransactionBase Packet;
  Packet2 = DataPacket::type_id::create( "Data" );
  Packet1 = TokenPacket::type_id::create( "Token" );

  //Setup phase for the set address command
  wait_for_grant();
  void'( Packet1.randomize() with { Packet1.Pid == OUT; }
		);
  Packet1.CalCrc5();
  send_request( Packet1 );
  wait_for_item_done();
  wait_for_grant();
  void'( Packet2.randomize() with { Packet2.Pid == DATA0;});
  add = 64'h00_05_01_00_00_00_00_00;
  Packet2.Data = new[64];
  foreach(add[i]) begin
    Packet2.Data[i] = add[i];
  end
  Packet2.CalCrc16();
  send_request( Packet2 );
  ovm_report_info(get_type_name,"Sending Command Address...",OVM_MEDIUM);
  wait_for_item_done();
  ovm_report_info(get_type_name,"Received acknowledgement from the device...",OVM_MEDIUM);
  
 endtask : body


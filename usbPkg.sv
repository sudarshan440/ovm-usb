package usbPkg;
   //`include "ovm_pkg.sv"
    
   //`include "ovm_pkg.sv"
   //`include "ovm_macros.svh"
   typedef enum bit[7:0] { OUT   = 8'b10000111,   IN    = 8'b10010110, SOF   = 8'b10100101,
                          SETUP = 8'b10110100,   DATA0 = 8'b11000011, DATA1 = 8'b11010010,
                          DATA2 = 8'b11100001,   MDATA = 8'b11110000, ACK   = 8'b01001011,
                          NAK   = 8'b01011010,   SPLIT = 8'b00011110, NYET  = 8'b01101001,
                          PRE   = 8'b00111100,   STALL = 8'b01111000, PING  = 8'b00101101 } Pid_t;

  `include "ovm.svh"
    
  `include "TransactionBase.sv"
   
  `include "TokenPacket.sv"
  `include "DataPacket.sv"
  `include "HandShakePacket.sv"
  `include "DArray.sv"
  `include "Wrapper.sv"
  `include "Get_Descriptor.sv"
  `include "Set_Address_Sequence.sv"
  `include "Status_Transaction.sv"
  `include "Token.sv"
  `include "usb_sequencer.sv"
  `include "BitStuffer.sv"
  `include "BitDestuffer.sv"
  `include "NrziEncoder.sv"
  `include "NrziDecoder.sv"
  `include "PacketAnalyzer.sv"
  `include "PacketFormatter.sv"
  `include "SignalDrive.sv"
  `include "ResponseBack.sv"
  
  `include "usb_driver.sv"
  `include "usb_monitor.sv"
  `include "usb_agent.sv"
  `include "scoreboard.sv"
  `include "usb_envirnoment.sv"
endpackage : usbPkg

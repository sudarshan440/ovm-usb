class BitStuffer extends ovm_component;
  ovm_blocking_put_port#( DArray ) Put;
  ovm_blocking_get_port#( DArray ) Get;

  `ovm_component_utils_begin( BitStuffer )
  `ovm_component_utils_end

  extern function new( string name = "", ovm_component parent = null );
  extern function void build();
  extern task run();
  
endclass : BitStuffer

function BitStuffer::new( string name = "", ovm_component parent = null );
  super.new( name, parent );
endfunction :new 

function void BitStuffer::build();
  apply_config_settings();
  Put = new("Put",this);
  Get = new("Get",this);
  super.build();
endfunction : build

task BitStuffer::run();
  DArray tmp,tmp1;
  bit flag = 1'b0;
  bit stream[$] = {};
  int count = 0;
  forever begin
    flag = 1'b0;
    stream = '{};
    count = 0;
    Get.get(tmp);
    //$display("%d",get_type_name,tmp.BitArray.size);
    ovm_report_info(get_type_name,$psprintf("Bit stream aquired"),OVM_HIGH);
    for(int i = 0;i < tmp.BitArray.size;i++) begin
      if( count == 6) begin
        count = 0;
        flag =1'b1;
        stream.push_back(1'b0);
      end
      if( tmp.BitArray[i] == 1'b1 ) begin
        count++;
        stream.push_back(1'b1);
      end else if( tmp.BitArray[i] == 1'b0 ) begin
        count = 0;
        stream.push_back(1'b0);
      end 
    end
    tmp1 = DArray::type_id::create( "tmp1" );
    tmp1.BitArray = stream;
    //$display("%d",get_type_name,tmp1.BitArray.size);
    Put.put( tmp1 );
    if(flag == 1'b0) begin
      ovm_report_info(get_type_name,$psprintf("Sending unchanged Bit stream to Nrzi Encoder"),OVM_HIGH);
    end else begin
      ovm_report_info(get_type_name,$psprintf("Sending changed Bit stream to Nrzi Encoder"),OVM_HIGH);
    end
  end
endtask  : run


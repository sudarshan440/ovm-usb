class NrziDecoder extends ovm_component;
  ovm_blocking_put_port#( DArray ) Put;
  ovm_blocking_get_port#( DArray ) Get;
  bit LastBit = 1'b1;

  `ovm_component_utils_begin( NrziDecoder )
  `ovm_component_utils_end

  extern function new( string name = "", ovm_component parent = null );
  extern function void build();
  extern task run();
  
endclass : NrziDecoder

function NrziDecoder::new( string name = "",ovm_component parent = null );
  super.new(name,parent);
endfunction : new

function void NrziDecoder::build();
  Put = new("Put",this);
  Get = new("Get",this);
  super.build();
endfunction : build

task NrziDecoder::run();
  DArray tmp;
  DArray tmp1;
  bit stream[$];
  forever begin
    Get.get( tmp );
    LastBit = 1'b1;
    ovm_report_info(get_type_name,$psprintf("Bit stream acquired"),OVM_HIGH);
    for(int i = 0;i<tmp.BitArray.size;i++) begin
      if(tmp.BitArray[i] == LastBit) begin
        stream.push_back( 1'b1 );
        LastBit = tmp.BitArray[i];
      end else begin
        stream.push_back( 1'b0 );
        LastBit = tmp.BitArray[i];
      end
    end
      tmp1 = DArray::type_id::create( "tmp1" );
      tmp1.BitArray = new[stream.size-8]( stream[8:$] );
      //$display("%s %p",get_type_name,stream[8:15]);
      Put.put( tmp1 );
      stream = {};
      ovm_report_info(get_type_name,$psprintf("Sending Decoded Bit stream to Bit Destuffer"),OVM_HIGH); 
  end
endtask : run 


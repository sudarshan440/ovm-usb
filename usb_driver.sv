  
 class usb_driver extends ovm_driver#(TransactionBase);

  PacketAnalyzer  pa;
  BitStuffer      bs;
  NrziEncoder     ne;
  SignalDrive     sd;
  ResponseBack    rb;
  NrziDecoder     nd;
  BitDestuffer    bd;
  PacketFormatter pf;
  tlm_fifo #(TransactionBase) t1;
  tlm_fifo #(DArray) t2;
  tlm_fifo #(DArray) t3;
  tlm_fifo #(DArray) t4;
  tlm_fifo #(DArray) t5;
  tlm_fifo #(DArray) t6;
  tlm_fifo #(DArray) t7;
  tlm_fifo #(TransactionBase) t8;




  `ovm_component_utils_begin( usb_driver )
  
  `ovm_component_utils_end

  extern function new(string name = "", ovm_component parent = null);
  extern function void build();
  extern function void connect();
  extern task run();
  
  
endclass : usb_driver

function usb_driver::new( string name = "", ovm_component parent = null );
  super.new(name,parent);
endfunction : new

function void usb_driver::build();
  super.build();
  pa     = PacketAnalyzer::type_id::create("pa",this);
  bs     = BitStuffer::type_id::create("bs",this);
  ne     = NrziEncoder::type_id::create("ne",this);
  sd     = SignalDrive::type_id::create("sd",this);
  rb     = ResponseBack::type_id::create("rb",this);
  nd     = NrziDecoder::type_id::create("nd",this);
  bd     = BitDestuffer::type_id::create("bd",this);
  pf     = PacketFormatter::type_id::create("pf",this);
  t1     = new("t1",this);  
  t2     = new("t2",this);
  t3     = new("t3",this);
  t4     = new("t4",this);
  t5     = new("t5",this);
  t6     = new("t6",this);
  t7     = new("t7",this);
  t8     = new("t8",this);

endfunction : build

function void usb_driver::connect();
  pa.Get.connect(t1.blocking_get_export);
  pa.Put.connect(t2.blocking_put_export);
  bs.Get.connect(t2.blocking_get_export);
  bs.Put.connect(t3.blocking_put_export);
  ne.Get.connect(t3.blocking_get_export);
  ne.Put.connect(t4.blocking_put_export);
  sd.Get.connect(t4.blocking_get_export);
  rb.Put.connect(t5.blocking_put_export);
  nd.Get.connect(t5.blocking_get_export);
  nd.Put.connect(t6.blocking_put_export);
  bd.Get.connect(t6.blocking_get_export);
  bd.Put.connect(t7.blocking_put_export);
  pf.Get.connect(t7.blocking_get_export);
  pf.Put.connect(t8.blocking_put_export); 
endfunction : connect

task usb_driver::run();
  TransactionBase tmp,tmp1;
  TokenPacket Packet1;
  DataPacket Packet2;
  HandShakePacket Packet3;
  bit Response;
  forever begin
      Response = 1'b0;
      //Starting Token 
      seq_item_port.get_next_item( tmp );
      ovm_report_info(get_type_name,$psprintf("Sequence sending %s...",tmp.get_name),OVM_MEDIUM);
      t1.put( tmp );
      if(!$cast(Packet1,tmp)) begin
        ovm_report_error(get_type_name,$psprintf("Casting failed"),OVM_MEDIUM);
      end else begin
        if(Packet1.Pid == SETUP || Packet1.Pid == OUT ) begin
          Response = 1'b1;
          seq_item_port.item_done();
        end else  begin
          Response = 1'b0;
          t8.get(tmp1);
          ovm_report_info(get_type_name,$psprintf("Sequence received %s...",tmp1.get_name),OVM_MEDIUM);
          seq_item_port.item_done();
        end
      end
      if(Response == 1'b1) begin
        seq_item_port.get_next_item( tmp);
        ovm_report_info(get_type_name,$psprintf("Sequence sending %s...",tmp.get_name),OVM_MEDIUM);
        t1.put( tmp);
        t8.get( tmp1);
        ovm_report_info(get_type_name,$psprintf("Sequence received %s...",tmp1.get_name),OVM_MEDIUM);
        seq_item_port.item_done( );
      end else begin
        seq_item_port.get_next_item(tmp);
        if(!$cast(Packet3,tmp)) begin
          $display("cast failed");
        end
        ovm_report_info(get_type_name,$psprintf("Sequence sending %s...",tmp.get_name),OVM_MEDIUM);
        t1.put(tmp);
        seq_item_port.item_done();
      end
  end
endtask : run




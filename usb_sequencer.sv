class usb_sequencer extends ovm_sequencer#(TransactionBase);

//registering
`ovm_component_utils(usb_sequencer)

//constructor
function new(string name = " ", ovm_component parent = null);
super.new(name,parent);
endfunction : new

endclass : usb_sequencer
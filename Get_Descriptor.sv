  
class Get_Descriptor extends ovm_sequence#(TransactionBase);
  `ovm_object_utils_begin( Get_Descriptor )
  `ovm_object_utils_end

  extern function new( string name = "" );
  extern task body();
  
endclass : Get_Descriptor

function Get_Descriptor::new( string name = "" );
  super.new( name );
endfunction : new

task Get_Descriptor::body();
  TokenPacket Packet1;                                                                                       
  bit [0:63] con;
  DataPacket Packet2;
  HandShakePacket Packet3;
  TransactionBase Packet;
  Packet2 = DataPacket ::type_id::create( "Data"  );
  Packet1 = TokenPacket::type_id::create( "Token" );
  
  //setup phase for the get descriptor commands
  wait_for_grant();
  void'( Packet1.randomize() with { Packet1.Pid == SETUP;
                                    Packet1.Addr == 7'b0000001;
                                    Packet1.Endp == 4'b0000;    });
  Packet1.CalCrc5();
  send_request( Packet1 );
  wait_for_item_done();
  wait_for_grant();
  void'( Packet2.randomize() with { Packet2.Pid == DATA0;});
  con = 64'h80_06_01_00_00_00_01_12;
  Packet2.Data = new[64];
  foreach( con[i] ) begin
    Packet2.Data[i] = con[i];
  end
  Packet2.CalCrc16();
  send_request( Packet2 );
  ovm_report_info(get_type_name,"Sending Command Get descriptor...",OVM_MEDIUM);
  wait_for_item_done();
  ovm_report_info(get_type_name,"Received acknowledgement from the device...",OVM_MEDIUM);
  
  //Data Phase for the get configuration commands
 
  wait_for_grant();
  Packet1 = TokenPacket::type_id::create( "Token" );
  void'( Packet1.randomize() with { Packet1.Pid == IN;
                                    Packet1.Addr == 7'b0000001;
                                    Packet1.Endp == 4'b0000;    });
  Packet1.CalCrc5();
  send_request( Packet1 );
  wait_for_item_done();

   Packet3 = HandShakePacket::type_id::create( "Token" );
 
  get_response(Packet3) ;

  if (Packet3.Pid == ACK)
  ovm_report_info(get_type_name,"Received acknowledgement from the device...",OVM_MEDIUM);
  else 
  ovm_report_info(get_type_name,"No acknowledgement from the device...",OVM_MEDIUM);

endtask : body


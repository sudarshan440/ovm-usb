class BitDestuffer extends ovm_component;
  ovm_blocking_put_port#( DArray ) Put;
  ovm_blocking_get_port#( DArray ) Get;

  `ovm_component_utils_begin( BitDestuffer )
  `ovm_component_utils_end

  extern function new ( string name = "", ovm_component parent = null );
  extern function void build();
  extern task run();
  
endclass : BitDestuffer

function BitDestuffer::new( string name = "", ovm_component parent = null );
  super.new(name,parent);
endfunction : new

function void BitDestuffer::build();
  apply_config_settings();
  Put = new("Put",this);
  Get = new("Get",this);
  super.build();
endfunction : build

task BitDestuffer::run();
  DArray tmp,tmp1;
  bit flag ;
  bit stream[$];
  int unsigned count;
  forever begin
    stream = {};
    count = 0;
    flag = 1'b0;
    Get.get( tmp );
    ovm_report_info(get_type_name,$psprintf("Bit stream acquired"),OVM_HIGH);
    
    for( int i= 0;i < tmp.BitArray.size;i++) begin
      if(count == 6 ) begin
        count = 0;
        flag = 1'b1;
        continue;
      end 
      if(tmp.BitArray[i] == 1'b1) begin
        count++;
        stream.push_back(1'b1);
      end else begin
        count = 0;
        stream.push_back(1'b0);
      end
    end
    tmp1 = DArray::type_id::create( "tmp1" );
    tmp1.BitArray = new[stream.size](stream);
    Put.put( tmp1 );
    if( flag == 1'b1) begin
      ovm_report_info(get_type_name,$psprintf("Bit destuffing performed and sent for formatter"),OVM_HIGH);
    end else begin
      ovm_report_info(get_type_name,$psprintf("No stuffing found and sent for formatter"),OVM_HIGH);
    end
  end
endtask : run


class PacketFormatter extends ovm_component;
  ovm_blocking_put_port#( TransactionBase ) Put;
  ovm_blocking_get_port#( DArray ) Get;
  bit inp;

  `ovm_component_utils_begin( PacketFormatter )
  `ovm_component_utils_end

  extern function new( string name = "", ovm_component parent = null);
  extern function void build();
  extern task run();
  
endclass : PacketFormatter

function PacketFormatter::new( string name = "", ovm_component parent = null );
  super.new(name,parent);
endfunction : new

function void PacketFormatter::build();
  apply_config_settings();
  Put = new("Put",this);
  Get = new("Get",this);
  super.build();
endfunction : build

task PacketFormatter::run();
  DArray tmp;
  Pid_t  Pid;
  byte pid;
  bit stream[$];
  forever begin
    stream = {};
    Get.get( tmp );
    stream = tmp.BitArray;
      for( int i =7;i>=0;i--) begin
        pid[i] = stream.pop_front();
      end
    Pid = Pid_t'(pid);
    ovm_report_info(get_type_name,$psprintf("Got the Pid %s...........",Pid),OVM_MEDIUM);
    if( Pid inside { DATA0,DATA1,DATA2,MDATA }) begin
      DataPacket data;
      data = DataPacket::type_id::create( "Data");
      data.Pid = Pid;
      for(int i=15;i>=0;i--) begin
        data.Crc16[i] = stream.pop_back;
      end
      data.Data = stream;
      //void'(data.unpack(tmp.BitArray));
      Put.put(data);
      ovm_report_info(get_type_name,$psprintf("Packet identified as data"),OVM_MEDIUM);
    end else if(Pid inside { OUT,IN,SETUP,SOF}) begin
      TokenPacket Token;
      Token = TokenPacket::type_id::create( "Token" );
      //void'(Token.unpack(tmp.BitArray));
      Token.Pid = Pid;
      for(int i=6;i>=0;i--) begin
        Token.Addr[i] = stream.pop_front();
      end
      for(int i=3;i>=0;i--) begin
        Token.Endp[i] = stream.pop_front();
      end
      for(int i=4;i>=0;i--) begin
        Token.Crc5[i] = stream.pop_front();
      end
      Put.put(Token);
      ovm_report_info(get_type_name,$psprintf("Packet identified as Token"),OVM_MEDIUM);
    end else if( Pid inside { ACK,NAK }) begin
      HandShakePacket HandShake;
      HandShake = HandShakePacket::type_id::create("HandShake");
      HandShake.Pid = Pid;
      //void'(HandShake.unpack(tmp.BitArray));
      Put.put(HandShake);
      ovm_report_info(get_type_name,$psprintf("Packet identified as handshake"),OVM_MEDIUM);
    end
  end
endtask : run


class HandShakePacket extends TransactionBase;
  rand Pid_t Pid;

  `ovm_object_utils_begin( HandShakePacket )
    `ovm_field_enum( Pid_t, Pid, OVM_ALL_ON )
  `ovm_object_utils_end

  extern function new ( string name = "");
  constraint Hand_Pid;
  
endclass : HandShakePacket

function HandShakePacket::new( string name = "" );
  super.new(name);
endfunction : new

constraint HandShakePacket::Hand_Pid{ Pid inside { ACK,NAK,STALL,NYET};}


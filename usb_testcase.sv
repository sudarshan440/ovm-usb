class usb_testcase extends ovm_test;
  Get_Descriptor GD;
  //Token token;
  Set_Address_Sequence SA;
  Status_Transaction ST;
  
  
  //Set_Configuration_Sequence SC;
  usb_sequencer sequencer;
  usb_environment env;
//registering
`ovm_component_utils(usb_testcase)

  extern function new ( string name = "", ovm_component parent = null );
  extern function void build();
  extern task run();


endclass : usb_testcase

//constructor
function usb_testcase::new(string name = "", ovm_component parent= null);
  super.new(name,parent);
endfunction : new

function void usb_testcase::build();
  super.build();
  env = usb_environment::type_id::create("env",this);
endfunction : build

task usb_testcase::run();
  $cast(sequencer,env.agent.sequencer);
  sequencer.count = 0;
  GD = Get_Descriptor::type_id::create("GD"); 
  GD.start(sequencer,null);
  SA = Set_Address_Sequence::type_id::create("SA"); 
  SA.start(sequencer,null);
//  token = Token::type_id::create("token"); 
 // token.start(sequencer,null);

  ST = Status_Transaction::type_id::create("SC"); 
  ST.start(sequencer,null);
endtask 

 
class PacketAnalyzer extends ovm_component;
  ovm_blocking_put_port#( DArray ) Put;
  ovm_blocking_get_port#( TransactionBase ) Get;

  `ovm_component_utils_begin( PacketAnalyzer )
  `ovm_component_utils_end

  extern function new( string name = "", ovm_component parent = null );
  extern function void build();
  extern task run();
  
endclass : PacketAnalyzer

function PacketAnalyzer::new( string name = "", ovm_component parent = null );
  super.new( name, parent);
endfunction : new

function void PacketAnalyzer::build();
  Put = new("Put",this);
  Get = new("Get",this);
  super.build();
endfunction : build

task PacketAnalyzer::run();
  TransactionBase tmp;
  TokenPacket token;
  DataPacket data;
  HandShakePacket handshake;
  DArray dtmp;
  bit temp[$];
  bit stream[];
  forever begin
    Get.get( tmp );
    //ovm_report_info(get_type_name,$psprintf("Packet aquired"),OVM_MEDIUM);
    if( tmp.get_name == "Token" ) begin
      if(!$cast(token,tmp)) begin
        ovm_report_error(get_type_name,$psprintf("Cast failed because Packet received is not a Token Packet"),OVM_HIGH);
      end else begin
        //void'(token.pack( stream ));
        temp = '{1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b1};
        for(int i=7;i>=0;i--) begin
          temp.push_back(token.Pid[i]);
        end
        for( int i=0;i<=6;i++) begin
          temp.push_back(token.Addr[i]);
        end
        for( int i=0;i<=3;i++) begin
          temp.push_back(token.Endp[i]);
        end
        for( int i=4;i>=0;i--) begin
          temp.push_back(token.Crc5[i]);
          //$write("%b",token.Crc5[i]);
        end
        dtmp = DArray::type_id::create( "dtmp" );
        dtmp.BitArray  = temp;
        Put.put( dtmp );
        ovm_report_info(get_type_name,$psprintf("Packet identified as Token and sent bit stream to Bit stuffer"),OVM_HIGH);
        temp = {};
      end
    end else if( tmp.get_name == "Data" ) begin
      if(!$cast(data,tmp)) begin
        ovm_report_error(get_type_name,$psprintf("Cast failed because Packet received is not a Data Packet"),OVM_HIGH);
      end else begin
        temp = '{1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b1};
        for(int i = 7;i>=0;i--) begin
          temp.push_back(data.Pid[i]);
        end
        for(int i = 1;i<=data.Data.size/8;i++) begin
          for( int j=0;j<=7;j++) begin
            temp.push_back(data.Data[(8*i)-1-j]);
          end
        //$display("%d",data.Data.size);
        end
        for(int i =15;i>=0;i--) begin
          temp.push_back(data.Crc16[i]);
        end
        dtmp = DArray::type_id::create( "dtmp" );
        dtmp.BitArray = temp;
        Put.put( dtmp );
        ovm_report_info(get_type_name,$psprintf("Packet identified as Data and sent bit stream to Bit stuffer"),OVM_HIGH);
        temp = {};
      end
    end else if(tmp.get_name == "HandShake") begin
      if(!$cast(handshake,tmp)) begin
        ovm_report_error(get_type_name,$psprintf("Cast failed because Packet received is not a HandShake Packet"),OVM_HIGH);
      end else begin
        //void'(handshake.pack( stream ));
        temp = '{1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b1};
        for(int i=7;i>=0;i--) begin
          temp.push_back(handshake.Pid[i]);
          //$write("%b",handshake.Pid[i]);
        end
        dtmp = DArray::type_id::create( "dtmp" );
        dtmp.BitArray  = temp;
        Put.put( dtmp );
        ovm_report_info(get_type_name,$psprintf("Packet identified as HandShake and sent bit stream to Bit stuffer"),OVM_HIGH);
        temp = {};
      end
    end
  end
endtask : run


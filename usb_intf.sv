interface usb_intf;
  logic clk;
  logic reset;
  logic usb_p_in;
  logic usb_n_in;
  logic usb_p_out;
  logic usb_n_out;
 
endinterface : usb_intf



class NrziEncoder extends ovm_component;
  ovm_blocking_put_port#( DArray ) Put;
  ovm_blocking_get_port#( DArray ) Get;
  bit LastBit = 1'b1;

  `ovm_component_utils_begin( NrziEncoder )
  `ovm_component_utils_end

  extern function new( string name = "", ovm_component parent = null );
  extern function void build();
  extern task run();
  
endclass : NrziEncoder

function NrziEncoder::new( string name = "", ovm_component parent = null );
  super.new( name , parent );
endfunction : new

function void NrziEncoder::build();
  Put = new("Put",this); 
  Get = new("Get",this);
  super.build();
endfunction : build

task NrziEncoder::run();
  DArray tmp;
  DArray tmp1;
  bit stream[$] = {};
  forever begin
    LastBit = 1'b1;
    Get.get( tmp );
    //$display("%s  %p",get_type_name,tmp.BitArray);
    ovm_report_info(get_type_name,$psprintf("Bit stream aquired"),OVM_HIGH);
    for(int i = 0; i< tmp.BitArray.size;i++) begin
      if( tmp.BitArray[i] == 1'b0 ) begin
        stream.push_back(~LastBit); 
        LastBit = ~LastBit;
      end else begin
        stream.push_back( LastBit );
        LastBit = LastBit;
      end
    end
    tmp1 = DArray::type_id::create( "tmp1" );
    tmp1.BitArray = stream;
    Put.put( tmp1 );
    //$display("%s  %p",get_type_name,tmp1.BitArray);
    stream = {};
    ovm_report_info(get_type_name,$psprintf("Sending Encoded bit stream to Signal Drive"),OVM_HIGH);
  end
endtask : run



module top;
  import usbPkg::*;
  import usbTestPkg::*;
usb_intf intf();


initial
begin
intf.clk = 0;
end
always #10 intf.clk = ~intf.clk;
/*
usb dut (.clk(intf.clk),
                 .reset(intf.reset),
                 .usb_p_in(intf.usb_p_in),
                 .usb_n_in(intf.usb_n_in),
				 .usb_p_out(intf.usb_p_out),
				 .usb_n_out(intf.usb_n_out)
				 
		);

*/
initial
begin
Wrapper wrapper = new("Wrapper");
wrapper.setVintf(intf);

set_config_object("*","configuration",wrapper,0);
run_test("usb_testcase");
end


endmodule : top


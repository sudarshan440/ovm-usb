 
class Set_Address_Sequence extends ovm_sequence #( TransactionBase );
  `ovm_object_utils_begin( Set_Address_Sequence )
  `ovm_object_utils_end

  extern function new( string name = "" );
  extern task body();
  
  
endclass : Set_Address_Sequence

function Set_Address_Sequence::new( string name = "" );
  super.new( name );
endfunction : new

task Set_Address_Sequence::body();
  TokenPacket Packet1;
  bit [0:63] add;
  DataPacket Packet2;
  HandShakePacket Packet3;
  TransactionBase Packet;
  Packet2 = DataPacket::type_id::create( "Data" );
  Packet1 = TokenPacket::type_id::create( "Token" );

  //out phase for the set address command
  wait_for_grant();
  void'( Packet1.randomize() with { Packet1.Pid == OUT;
                                   Packet1.Addr == 7'b0000000;
                                   Packet1.Endp == 4'b0000;    });
  Packet1.CalCrc5();
  send_request( Packet1 );
  wait_for_item_done();
  wait_for_grant();
  void'( Packet2.randomize() with { Packet2.Pid == DATA0;});
  add = 64'h00_05_03_00_00_00_00_00;
  Packet2.Data = new[64];
  foreach(add[i]) begin
    Packet2.Data[i] = add[i];
  end
  Packet2.CalCrc16();
  send_request( Packet2 );
  ovm_report_info(get_type_name,"Sending Command Address...",OVM_MEDIUM);
  wait_for_item_done();
  
  ovm_report_info(get_type_name,"Received acknowledgement from the device...",OVM_MEDIUM);
  // Status phase for the set_address command
  Packet2 = DataPacket::type_id::create("Data");
  Packet1 = TokenPacket::type_id::create("Token");
  wait_for_grant();
  get_response(Packet);
  void'(Packet1.randomize() with { Packet1.Pid == IN;
                                   Packet1.Addr == 7'b0000000;
                                   Packet1.Endp == 4'b0000;    });
  Packet1.CalCrc5();
  send_request( Packet1 );
  wait_for_item_done();
//  ovm_report_info(get_type_name,"Received Data from the device from the device...",OVM_MEDIUM);

Packet3 = HandShakePacket::type_id::create( "Token" );
  get_response( Packet3 );
   if (Packet3.Pid == ACK)
  ovm_report_info(get_type_name,"Received acknowledgement from the device...",OVM_MEDIUM);
  else if (Packet3.Pid == NAK)
  ovm_report_info(get_type_name,"No data or device is busy ...",OVM_MEDIUM);
  else if (Packet3.Pid == STALL)
  ovm_report_info(get_type_name,"unsupported format ...",OVM_MEDIUM);


 endtask : body


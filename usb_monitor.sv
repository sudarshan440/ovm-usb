class usb_monitor extends ovm_monitor;

  ResponseBack    rb1;
  NrziDecoder     nd1;
  BitDestuffer    bd1;
  PacketFormatter pf1;
  ResponseBack    rb2;
  NrziDecoder     nd2;
  BitDestuffer    bd2;
  PacketFormatter pf2;

  tlm_fifo #(TransactionBase) t1;
  tlm_fifo #(DArray) t2;
  tlm_fifo #(DArray) t3;
  tlm_fifo #(DArray) t4;
  tlm_fifo #(DArray) t5;
  tlm_fifo #(DArray) t6;
  tlm_fifo #(DArray) t7;
  tlm_fifo #(TransactionBase) t8;
  tlm_fifo #(TransactionBase) t9;
  tlm_fifo #(TransactionBase) t10;

  ovm_analysis_port #(TransactionBase) Put1;
   ovm_analysis_port #(TransactionBase) Put2;
  
  `ovm_component_utils_begin( usb_monitor )
  `ovm_component_utils_end

  extern function new( string name = "",ovm_component parent = null );
  extern function void build();
  extern function void connect();
  extern task run();
  
endclass : usb_monitor

function usb_monitor::new( string name = "", ovm_component parent = null);
  super.new(name,parent);
endfunction : new

function void usb_monitor::build();
  super.build();
  rb1 = new("rb1",this);
  nd1 = new("nd1",this);
  bd1 = new("bd1",this);
  pf1 = new("pf1",this);
  rb2 = new("rb2",this);
  nd2 = new("nd2",this);
  bd2 = new("bd2",this);
  pf2 = new("pf2",this);
  t1  = new("t1",this);
  t2  = new("t2",this);
  t3  = new("t3",this);
  t4  = new("t4",this);
  t5  = new("t5",this);
  t6  = new("t6",this);
  t7  = new("t7",this);
  t8  = new("t8",this);
  
  Put1  = new("Put1",this);
   Put2  = new("Put2",this);
  rb2.inp = 1'b0;
  rb1.inp = 1'b1;
  pf1.inp = 1'b0;
  pf2.inp = 1'b1;
endfunction: build

function void usb_monitor::connect();
  pf1.Put.connect(t1.blocking_put_export);
  pf1.Get.connect(t2.blocking_get_export);
  bd1.Put.connect(t2.blocking_put_export);
  bd1.Get.connect(t3.blocking_get_export);
  nd1.Put.connect(t3.blocking_put_export);
  nd1.Get.connect(t4.blocking_get_export);
  rb1.Put.connect(t4.blocking_put_export);
  pf2.Put.connect(t8.blocking_put_export);
  pf2.Get.connect(t7.blocking_get_export);
  bd2.Put.connect(t7.blocking_put_export);
  bd2.Get.connect(t6.blocking_get_export);
  nd2.Put.connect(t6.blocking_put_export);
  nd2.Get.connect(t5.blocking_get_export);
  rb2.Put.connect(t5.blocking_put_export);
   
endfunction : connect

task usb_monitor::run();
  TransactionBase tmp,tmp1;
  DataPacket Packet1;
  HandShakePacket Packet;
  TokenPacket Packet2;
   fork
    forever begin
      t1.get(tmp);
      Put1.write( tmp );
      if(tmp.get_name == "HandShake") begin
        if(!$cast(Packet,tmp)) begin
          ovm_report_error(get_type_name,"casting failed for the handshake Packet",OVM_MEDIUM);
        end else  begin
          ovm_report_info(get_type_name,$psprintf("Got %s in drive interface",Packet.Pid),OVM_MEDIUM);
        end
      end else if(tmp.get_name == "Data") begin
        if(!$cast(Packet1,tmp)) begin
          ovm_report_error(get_type_name,"casting failed for the Data Packet",OVM_MEDIUM);
        end else begin
          ovm_report_info(get_type_name,$psprintf("Got %s in drive interface",Packet1.Pid),OVM_MEDIUM);
        end
      end else if(tmp.get_name == "Token") begin
        if(!$cast(Packet2,tmp)) begin
          ovm_report_error(get_type_name,"casting failed for the Data Packet",OVM_MEDIUM);
        end else begin
          ovm_report_info(get_type_name,$psprintf("Got %s in drive interface",Packet2.Pid),OVM_MEDIUM);
        end
      end
    end
	 
    forever begin
      t8.get(tmp1);
      Put2.write(tmp1);
      if(tmp1.get_name == "HandShake") begin
        if(!$cast(Packet,tmp1)) begin
          ovm_report_error(get_type_name,"casting failed for the handshake Packet",OVM_MEDIUM);
        end else  begin
          ovm_report_info(get_type_name,$psprintf("Got %s in response interface",Packet.Pid),OVM_MEDIUM);
        end
      end else if(tmp1.get_name == "Data") begin
        if(!$cast(Packet1,tmp1)) begin
          ovm_report_error(get_type_name,"casting failed for the Data Packet",OVM_MEDIUM);
        end else begin
          ovm_report_info(get_type_name,$psprintf("Got %s in response interface",Packet1.Pid),OVM_MEDIUM);
        end
      end
      //Put2.put(tmp1);
    end
	 
   join
endtask : run




class ResponseBack extends ovm_component;
  
  ovm_blocking_put_port#( DArray ) Put;
    virtual usb_intf intf;
Wrapper wrapper;
ovm_object dummy;

  bit inp = 1'b0;
  `ovm_component_utils_begin( ResponseBack )
    `ovm_component_utils_end

  extern function new( string name = "", ovm_component parent = null );
  extern function void build();
  extern task run();
  
endclass : ResponseBack

function ResponseBack::new( string name = "", ovm_component parent = null);
  super.new( name, parent );
endfunction :  new


function void ResponseBack::build();
if(!get_config_object("configuration",dummy,0))
	ovm_report_info(get_type_name(),"dummy is not assigned",OVM_LOG);
else
	ovm_report_info(get_type_name(),"dummy is assigned",OVM_LOG); 


if($cast(wrapper,dummy))
  begin
	ovm_report_info(get_type_name(),"object configured properly",OVM_LOG);
	intf = wrapper.intf;
  end
else
	ovm_report_info(get_type_name(),"dummy is really dummy");

   Put = new("Put",this);
endfunction : build

task ResponseBack::run();
  DArray tmp1;
  bit stream[$] = {};
  int i=0;
  int j =0;
    

  forever begin
    j = 0;
    if(inp) begin
      @(negedge intf.usb_p_in,posedge intf.usb_n_in);
      while(1) begin
        @(posedge intf.clk)
        if( intf.usb_p_in == 1'b0 && intf.usb_n_in == 1'b0 ) begin
          j++;
          continue;
        end
        if( j == 2 ) begin
          if( intf.usb_p_in == 1'b1 && intf.usb_n_in == 1'b0 ) begin
            j = 0;
            break;
          end else begin
            j = 0;
          end
        end else begin
          stream.push_back(intf.usb_p_in);
        end
      end
      tmp1 = DArray::type_id::create( "tmp1");
      tmp1.BitArray = stream;
      Put.put( tmp1 );
      ovm_report_info(get_type_name,"Got response",OVM_HIGH);
      stream = {};
    end else begin
      @(negedge intf.usb_p_out,posedge intf.usb_n_out);
      //ovm_report_info(get_type_name,"icross ",OVM_MEDIUM);
      while(1) begin
        @(posedge intf.clk)
        if( intf.usb_p_out == 1'b0 && intf.usb_n_out == 1'b0 ) begin
          j++;
          continue;
        end
        if( j == 2 ) begin
          if( intf.usb_p_out == 1'b1 && intf.usb_n_out == 1'b0 ) begin
            j = 0;
            break;
          end else begin
            j = 0;
          end
        end else begin
          //$display("dfsfsfSF");
          stream.push_back(intf.usb_p_out);
        end
      end
      tmp1 = DArray::type_id::create( "tmp1");
      tmp1.BitArray = new[stream.size](stream);
      Put.put( tmp1 );
      ovm_report_info(get_type_name,"Got response",OVM_HIGH);
      stream = {};
    end
  end
endtask : run


 
class scoreboard extends ovm_scoreboard;
 // ovm_blocking_get_port# ( TransactionBase ) Get1;
  // ovm_blocking_get_port# ( TransactionBase ) Get2;
  
  TransactionBase datafromwrite;
TransactionBase datafromread;
ovm_analysis_export #(TransactionBase) writesb;
ovm_analysis_export #(TransactionBase) readsb;
tlm_analysis_fifo #(TransactionBase) getwrite;
tlm_analysis_fifo #(TransactionBase) getread;

  `ovm_component_utils_begin( scoreboard )
  `ovm_component_utils_end

  extern function new( string name = "", ovm_component parent = null );
  extern function void build();
  extern function void connect();
  extern task run();
  
  
endclass : scoreboard

function scoreboard::new( string name = "", ovm_component parent = null );
  super.new( name,parent );
  datafromwrite = new();
datafromread = new();

endfunction : new

function void scoreboard::build();
  //Get1 = new( "Get1",this );
  //Get2 = new( "Get2",this );
  super.build();
  writesb= new("writesb",this);
 readsb= new("readsb",this);
 getwrite= new("getwrite",this);
 getread= new("getread",this);

endfunction : build


function void scoreboard::connect();
writesb.connect(getwrite.analysis_export); //dut
readsb.connect(getread.analysis_export); //dut
endfunction : connect



task scoreboard::run();
  //TransactionBase Packet;
  TokenPacket Packet1;
  DataPacket Packet2;
  HandShakePacket Packet3;
  bit setup_done = 1'b0;
  bit data_done = 1'b0;
  bit data_phase_required = 1'b0;
  
  forever begin
    getwrite.get( datafromwrite );
//    $display("Iam here");
    if( $cast( Packet1,datafromwrite ) ) begin
      if( Packet1.Pid == SETUP ) begin
       // $display("sdadad");
        getwrite.get( datafromwrite );
        if( $cast( Packet2,datafromwrite ) ) begin
          if( Packet2.Pid == DATA0 ) begin
//            $display("%p",Packet2.Data);
            if( Packet2.Data[48] != 1'b0 || Packet2.Data[49] != 1'b0 ) begin
              data_phase_required = 1'b1;
            end
            getread.get( datafromread );
            if( $cast( Packet3,datafromread ) ) begin
              if( Packet3.Pid == ACK ) begin
                ovm_report_info( get_type_name,"############## SETUP PHASE SUCCESSFULL ###############",OVM_LOW );
                setup_done = 1'b1;
              end else begin
                ovm_report_info( get_type_name,"############## SETUP PHASE UNSUCCESSFULL #############",OVM_LOW );
              end
            end else begin
              ovm_report_error( get_type_name,"HandShake Packet is expected in the bus after Data Packet",OVM_LOW );
            end
          end
        end else begin
          ovm_report_error( get_type_name,"Data Packet is expected in the bus after Setup Packet",OVM_LOW );
        end
      end else if( Packet1.Pid == IN && setup_done ) begin
        setup_done = 1'b0;
        getread.get( datafromread );
        if( $cast( Packet2,datafromread ) ) begin
          if( Packet2.Pid == DATA0 || Packet2.Pid == DATA1 ) begin
            getwrite.get( datafromwrite );
            if( $cast( Packet3,datafromwrite ) ) begin
              if( Packet3.Pid == ACK ) begin
                if( data_phase_required ) begin
                  ovm_report_info( get_type_name,"############## DATA PHASE SUCCESSFULL ###############",OVM_LOW );
                  data_done = 1'b1;
                  data_phase_required = 1'b0;
                end else begin
                  ovm_report_info( get_type_name,"############## STATUS PHASE SUCCESSFULL ###############",OVM_LOW );
                end
              end else begin
                if( data_phase_required ) begin
                  ovm_report_info( get_type_name,"############## DATA PHASE UNSUCCESSFULL #############",OVM_LOW );
                  data_phase_required = 1'b0;
                end else begin
                  ovm_report_info( get_type_name,"############## STATUS PHASE UNSUCCESSFULL #############",OVM_LOW );
                end
              end
            end else begin
              ovm_report_error( get_type_name,"HandShake Packet is expected in the bus after Data Packet",OVM_LOW );
            end
          end 
        end else begin
          ovm_report_error( get_type_name,"Data Packet is expected in the bus after out Packet",OVM_LOW );
        end
     end else if( Packet1.Pid == OUT ) begin
       setup_done = 1'b0;
       getwrite.get( datafromwrite );
       if( $cast( Packet2,datafromwrite ) ) begin
         if( Packet2.Pid == DATA1 ) begin
           getread.get( datafromread );
           if( $cast( Packet3,datafromread ) ) begin
             if( Packet3.Pid == ACK ) begin
                 ovm_report_info( get_type_name,"############## STATUS PHASE SUCCESSFULL ###############",OVM_LOW );
             end else begin
                 ovm_report_info( get_type_name,"############## STATUS PHASE UNSUCCESSFULL #############",OVM_LOW );
             end
           end else begin
             ovm_report_error( get_type_name,"HandShake Packet is expected in the bus after Data Packet",OVM_LOW );
           end
         end 
       end else begin
         ovm_report_error( get_type_name,"Data Packet is expected in the bus after out Packet",OVM_LOW );
       end
    end
  end 
  end
endtask : run

